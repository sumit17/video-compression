package com.example.videocompression;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.bt_compress).setOnClickListener(view -> checkPermission());
    }

    private void checkPermission() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permission != PackageManager.PERMISSION_GRANTED) {
            String PERMISSIONS_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            ActivityCompat.requestPermissions(
                    this,
                    new String[] {PERMISSIONS_STORAGE},
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            continueAfterPermission();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_EXTERNAL_STORAGE) {
            int grantResult = grantResults[0];
            if (PackageManager.PERMISSION_GRANTED == grantResult) {
                continueAfterPermission();
            }
        }
    }

    private void continueAfterPermission() {
        progressDialog = dialogInit();
        progressDialog.show();
        final File sdDir = Environment.getExternalStorageDirectory();
        final String filePath = sdDir + "/video.mp4";
        new VideoCompressor(filePath).execute();
    }

    private ProgressDialog dialogInit() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Compressing");
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public class VideoCompressor extends AsyncTask<String, String, String> {

        private String filePath;

        VideoCompressor(String filePath) {
            this.filePath = filePath;
        }

        @Override
        protected String doInBackground(String... paths) {
            return MediaController.getInstance().convertVideo(filePath);

        }

        @Override
        protected void onPostExecute(final String filePath) {
            dismissDialog();
            if (filePath == null) {
                Toast.makeText(MainActivity.this, "Compression failure", Toast.LENGTH_SHORT).show();
            } else if (filePath.equals("")) {
                Toast.makeText(MainActivity.this, "File should be less than 20MB", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, "Compression success", Toast.LENGTH_SHORT).show();
            }
        }

        void dismissDialog() {
            if (!isFinishing()
                    && progressDialog != null
                    && progressDialog.isShowing()) {
                progressDialog.dismiss();

            }
        }
    }

}
